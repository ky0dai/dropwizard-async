package com.ky0dai;

import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

@Path("v1")
public class DummyResource {
    private final Executor executor = Executors.newCachedThreadPool();
    private final Map<String, BlockingQueue<String>> asyncResponseBuffer = new HashMap<>();


    @GET
    @Path("/blah")
    @Produces(MediaType.TEXT_HTML)
    public String sayBlah() {
        return "blah blah blah asdfasdfs";
    }

    @POST
    @Path(("/value"))
    public String postValue(@HeaderParam("X-VAL-Key") String key, String value) {
        if(asyncResponseBuffer.containsKey(key)) {
            asyncResponseBuffer.get(key).offer(value);
        }
        return String.format("Key: %s, Value: %s", key, value);
    }

    @GET
    @Path("/readValue")
    @Produces(MediaType.TEXT_HTML)
    public void asyncGet(@HeaderParam("X-VAL-Key") String key, @Suspended final AsyncResponse resp) {
        executor.execute(new AsycLookerUpper(asyncResponseBuffer, key, resp));
    }

    @GET
    @Path("/bleh")
    @Produces(MediaType.TEXT_HTML)
    public void asyncGet(@Suspended final AsyncResponse resp) {
        executor.execute(() -> {
            try {
                Thread.sleep(1000);
                resp.resume("hello world");
            } catch (InterruptedException e) {
                resp.resume(e);
            }
        });
    }
}

class AsycLookerUpper implements Runnable {
    private Map<String, BlockingQueue<String>> asyncResponseBuffer;
    private final String key;
    private AsyncResponse resp;

    AsycLookerUpper(Map<String, BlockingQueue<String>> asyncResponseBuffer, String key, AsyncResponse resp) {
        this.asyncResponseBuffer = asyncResponseBuffer;
        this.key = key;
        this.resp = resp;
    }

    @Override
    public void run() {
        BlockingQueue<String> buffer = new ArrayBlockingQueue<>(2);
        asyncResponseBuffer.put(key, buffer);
        try {
            String poll = buffer.poll(30, TimeUnit.SECONDS);
            resp.resume(poll);

        } catch (InterruptedException e) {
            throw new InternalServerErrorException("Timed out waiting!");
        }
    }
}
