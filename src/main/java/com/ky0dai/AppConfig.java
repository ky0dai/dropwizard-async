package com.ky0dai;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

public class AppConfig extends Configuration {

    @JsonProperty("dummyString")
    private final String dummyString;


    public AppConfig(@JsonProperty("dummyString") String dummyString) {

        this.dummyString = dummyString;
    }

    public String getDummyString() {
        return dummyString;
    }
}