package com.ky0dai;

import io.dropwizard.Application;
import io.dropwizard.configuration.ConfigurationSourceProvider;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class App extends Application<AppConfig> {

    public static void main(String... args) throws Exception {
        new App().run(args);
    }

    public void initialize(Bootstrap<AppConfig> bootstrap) {
        ConfigurationSourceProvider configurationSourceProvider = bootstrap.getConfigurationSourceProvider();
        bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(configurationSourceProvider,
                new EnvironmentVariableSubstitutor(true)));
    }


    @Override
    public void run(AppConfig appConfig, Environment environment) {

        environment.healthChecks().register("dummy", new DummyHealthCheck());

        environment.jersey().register(new DummyResource());
    }
}

