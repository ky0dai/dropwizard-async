# Dropwizard Async


###What is this?
This is a PoC to demonstrate asynchronous request handling in Dropwizard!

This will allow us to free up resource threads for long-running requests. One useful application of this approach is building service gateways that allow their clients a RESTful interface, but delegate backend processing to asynchronous messaging.

How to start the dropwizard-async application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/JARNAME.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

